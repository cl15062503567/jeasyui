import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.topteam.ui.model.Column;
import org.topteam.ui.model.JsonSerializeConfig;

import java.util.Arrays;
import java.util.List;

/**
 * Created by JiangFeng on 2014/9/29.
 */
public class JsonTest {

    public static void main(String[] args){
        Column column = new Column();
        column.setField("aaa");
        column.setTitle("xxx");
        column.setWidth("100");

        List<Column> columns = Arrays.asList(column);
        System.out.println(JSON.toJSONString(columns, JsonSerializeConfig.mapping, SerializerFeature.UseSingleQuotes));
    }
}
