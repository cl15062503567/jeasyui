<%--
  Created by IntelliJ IDEA.
  User: 枫
  Date: 2014/8/7
  Time: 21:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="e" uri="org.topteam/easyui" %>

<e:templateOverride name="body">
    <p>动态加载进来的内容，包括控件渲染，以及事件</p>
    <e:button id="dy" text="Dynamic" classStyle="c-danger">
        <e:eventListener event="click" listener="dynamicBtnClick" />
    </e:button>

    <script>
        function dynamicBtnClick(){
            alert('dynamic load');
        }
    </script>
</e:templateOverride>

<%@include file="_template.jsp" %>
