<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>EasyUI for JSP</title>

    <!-- Bootstrap core CSS -->
    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Documentation extras -->
    <link href="static/css/docs.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="static/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
    <script src="static/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="static/css/carousel.css" rel="stylesheet">
</head>
<!-- NAVBAR
================================================== -->
<body>
<div class="navbar-wrapper">
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">JEasyUI</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">首页</a></li>
                        <li><a href="demo.jsp" target="_blank">演示</a></li>
                        <li><a href="document.jsp">文档</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">源码 <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Git@OSC</a></li>
                                <li><a href="#">Git@Coding</a></li>
                            </ul>
                        </li>
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li><a href="http://blog.getbootstrap.com"
                               onclick="_hmt.push(['_trackEvent', 'docv3-navbar', 'click', 'doc-home-navbar-blog'])"
                               target="_blank">关于</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>


<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="static/images/bg.JPG"
                 alt="First slide">

            <div class="container">
                <div class="carousel-caption">
                    <h1>EasyUI for JSP.</h1>

                    <p>基于EasyUI的封装一套JSP控件库, 通过严格的标签来简化EasyUI的使用, 学习成本以及书写更少的代码. 提供了友好的IDE提示支持, 直接绑定java对象到控件.</p>

                    <p><a class="btn btn-outline-inverse btn-lg" style="border-width:2px;border-color:white;" href="document.jsp" role="button">下载JEasyUI</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="static/images/bg2.jpg" alt="Second slide">

            <div class="container">
                <div class="carousel-caption">
                    <h1>More for JSP.</h1>

                    <p>通过jstl表达式绑定java对象到控件. 添加主动推送、图表、附件上传、富文本编辑器等控件.</p>

                    <p><a class="btn btn-outline-inverse btn-lg" style="border-width:2px;border-color:white;" href="document.jsp" role="button">下载JEasyUI</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- /.carousel -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
            <img class="img-circle"
                 src="static/images/code.png"
                 alt="Simple Use" style="width: 140px; height: 140px;">

            <h2>简单、易用</h2>

            <p>通过标签化控件来书写EasyUI，让代码更加简洁. 友好的IDE提示，降低使用学习成本. </p>

            <p><a class="btn btn-default" href="#" role="button">更多详情 &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <img class="img-circle"
                 src="static/images/box.png"
                 alt="Generic placeholder image" style="width: 140px; height: 140px;">

            <h2>高效、标准化</h2>

            <p>通过标签化的封装，可以方便的切换前端UI或版本. 企业的开发框架中，可以轻松的做到功能级别的复用. </p>

            <p><a class="btn btn-default" href="#" role="button">更多详情 &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <img class="img-circle"
                 src="static/images/more.png"
                 alt="Generic placeholder image" style="width: 140px; height: 140px;">

            <h2>不止简单</h2>

            <p>除了EasyUI，还提供主动推送、ECharts、附件上传、富文本编辑器以及一些常用的处理工具类. </p>

            <p><a class="btn btn-default" href="#" role="button">更多详情 &raquo;</a></p>
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->


    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h3 class="featurette-heading">标签化EasyUI开发. <span
                    class="text-muted">让在JSP中使用EasyUI更加简单.</span></h3>

            <p class="lead">通过标签库来规范代码，减少代码出错。提供统一的事件处理，统一由框架解决兼容性等问题，同时获得IDE友好的提示。</p>
        </div>
        <div class="col-md-5">
            <img class="featurette-image img-responsive" src="static/images/code2.png"
                 alt="Generic placeholder image">
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img class="featurette-image img-responsive" src="static/images/demos.jpg" 
                 alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
            <h3 class="featurette-heading">演示站、文档已经完善！
            </h3>

            <p class="lead">丰富的文档和大量的Demo已经完善，Demo源码见 <a href="http://git.oschina.net/for-1988/jeasyui-example" target="_blank"><code>http://git.oschina.net/for-1988/jeasyui-example</code></a></p>
        </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->


    <!-- FOOTER -->
    <footer class="bs-docs-footer" role="contentinfo">
      <div class="container">
        

        <p>该网站由<a href="http://my.oschina.net/FengJ">@FengJ</a>创建.</p>
        <p>关于EasyUI的使用授权，JEasyUI只是基于EasyUI的GPL版本构建，完全遵循<a href="http://www.gnu.org/licenses/gpl.txt">GPL协议</a>.</p>
        <ul class="bs-docs-footer-links muted">
          <li>当前版本： v1.0.0</li>
          <li>·</li>
          <li><a href="http://git.oschina.net/for-1988">GIT@OSC 仓库</a></li>
          <li>·</li>
          <li><a href="../about/">关于</a></li>
          <li>·</li>
          <li><a href="http://git.oschina.net/for-1988/jeasyui/issues">Issues</a></li>
          <li>·</li>
          <li><a href="http://git.oschina.net/for-1988/jeasyui/tags">历史版本</a></li>
        </ul>
      </div>
    </footer>
</div>
<!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
<script src="static/bootstrap/js/bootstrap.min.js"></script>
<script src="static/js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="static/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
