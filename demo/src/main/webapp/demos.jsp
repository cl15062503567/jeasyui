<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="org.topteam.ui.model.TreeNode" %>
<%@ page import="org.topteam.demo.DemoData" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: 枫
  Date: 2014/8/6
  Time: 21:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="e" uri="org.topteam/easyui" %>

<%
    DemoData demo = new DemoData();
%>

<%
    TreeNode node = new TreeNode();
    node.setId("1");
    node.setText("Root");
    for (int i = 1; i < 10; i++) {
        TreeNode sub = new TreeNode();
        sub.setId("1." + i);
        sub.setText("Node" + i);
        node.getChildren().add(sub);
    }
    List<TreeNode> nodeList = new ArrayList<TreeNode>();
    nodeList.add(node);
%>

<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>Complex Layout - jQuery EasyUI Demo</title>
    <script type="text/javascript" src="static/jquery.min.js"></script>
    <e:resources location="static/easyui1.4.1" theme="metro"/>
    <e:resource location="static" name="demo.css"/>
    <e:resource location="static/plupload" name="plupload.full.min.js"/>
    <e:resource location="static/font-awesome/css" name="font-awesome.css"/>
    <!--[if lt IE 9]>
    <e:resource location="static/font-awesome/css" name="font-awesome-ie7.min.css"/>
    <![endif]-->
    <script>
        function btnClick() {
            alert(1);
        }
        function eventClick() {
            alert('event');
        }

        function treeClick(node) {
            alert(node.text);
        }

    </script>
</head>
<e:body>
    <e:layout id="main" fit="true">
        <e:layoutUnit region="north" style="height:150px">
            <e:panel id="panel" title="Panel" maximizable="true">
                <e:button id="add-btn" text="Add" iconCls="icon-add" classStyle="c-red"/>
                <e:eventListener for="add-btn" event="click" listener="btnClick"/>
                <e:button id="link-btn" text="Link" iconCls="icon-save" href="http://baidu.com"/>
                <e:button id="dis-btn" text="Disabeld" disabled="true" classStyle="c-info"/>
                <e:button id="plain-btn" text="Plain" iconCls="icon-save" plain="true"/>
                <e:button id="large-btn" text="Large Chart" iconCls="icon-large-chart" iconAlign="top" size="large"/>
                <e:button id="event-btn" text="Event">
                    <e:eventListener event="click" listener="eventClick"/>
                    <e:event event="click" target="panel" action="maximize"/>
                </e:button>
                <e:menuButton id="menuBtn" text="menuBtn" iconCls="icon-help">
                    <e:menuitem text="Sub1"/>
                    <e:submenu label="Sub2">
                        <e:menuitem text="Sub2.1"/>
                    </e:submenu>
                </e:menuButton>
                <e:splitButton id="splitButton" text="splitButton" iconCls="icon-help">
                    <e:menuitem text="Sub1"/>
                    <e:submenu label="Sub2">
                        <e:menuitem text="Sub2.1"/>
                    </e:submenu>
                </e:splitButton>
                <br/>
                <e:button id="open-win" text="Open Window" classStyle="c-info">
                    <e:event event="click" target="window" action="open"/>
                </e:button>
                <e:button id="close-win" text="Close Window" classStyle="c-danger">
                    <e:event event="click" target="window" action="close"/>
                </e:button>
                <e:window id="window" title="Window" closable="true" collapsible="true" draggable="true" modal="true"
                          maximizable="true" style="width:500px;height:400px;">
                    <e:calendar id="cal" fit="true"/>
                </e:window>
            </e:panel>
            <e:eventListener for="panel" event="onMaximize" listener="btnClick"/>
        </e:layoutUnit>
        <e:layoutUnit region="west" style="width:300px" title="测试" split="true">
            <e:accordion id="acc-1" fit="true">
                <e:tab title="Accordion1">
                    <e:menu id="menu" show="true" target="acc-1">
                        <e:submenu label="Menu1">
                            <e:menuitem text="Menu1.1" href="http://www.baidu.com"/>
                        </e:submenu>
                        <e:menuitem text="Menu2"/>
                    </e:menu>
                </e:tab>
                <e:tab title="Tree">
                    <e:tree id="tree" data="<%=nodeList %>" checkbox="true">
                        <e:event event="onClick" target="txt1" action="setText">
                            <e:param expression="args[0].text"></e:param>
                        </e:event>
                    </e:tree>
                </e:tab>
                <e:tab title="Accordion2" href="dynamic.jsp">
                </e:tab>
            </e:accordion>
        </e:layoutUnit>
        <e:layoutUnit region="center" title="测试2">
            <e:tabs id="tabs1" fit="true">
                <e:tab title="测试tab" style="padding:20px;">
                    <e:datagrid id="grid" data="<%=demo.getProducts() %>" fit="true" title="Fluid DataGrid"
                                classStyle="c-red" pagination="true" fitColumns="false" pageList="10,20,40">
                        <e:columns frozen="true">
                            <e:column field="itemid" width="100">
                                Item ID
                            </e:column>
                            <e:column field="productid" width="200" sortable="true">Product</e:column>
                            <e:column field="listprice" width="200">List Price</e:column>
                            <e:column field="unitcost" width="100" title="单价"/>
                            <e:column field="attr1" width="200">Product</e:column>
                            <e:column field="status" width="200">List Price</e:column>
                        </e:columns>
                    </e:datagrid>
                </e:tab>
                <e:tab title="Tab2" iconCls="icon-man">
                    <e:treegrid id="treegrid" idField="id" treeField="name" url="treegrid_data1.json" rownumbers="true">
                        <e:columns>
                            <e:column field="name" width="220">Name</e:column>
                            <e:column field="size" width="100">Size</e:column>
                            <e:column field="date" width="150">Modified Date</e:column>
                        </e:columns>
                    </e:treegrid>
                </e:tab>
                <e:tab title="Form">
                    <e:panel id="f-panel" style="padding:10px;" collapsible="true" title="From Area" fit="true">
                        <e:form id="form1" classStyle="form-area">
                            <div class="input-control">
                                <label class="right">姓名：</label>
                                <e:inputText classStyle="long" id="txt1" required="true" iconCls="icon-man"
                                             iconWidth="36">
                                </e:inputText>
                                <label class="right">邮箱：</label>
                                <e:inputText classStyle="long" id="txt2" validType="email"/>
                            </div>
                            <div class="input-control">
                                <label class="right">密码：</label>
                                <e:inputText classStyle="long" id="psw" prompt="Password" iconCls="icon-lock"
                                             iconWidth="36" required="true" type="password"/>
                                <label class="right">邮箱：</label>
                                <e:validateInput classStyle="long" id="txt3" validType="email"/>
                            </div>
                            <div class="input-control">
                                <label class="right">性别：</label>
                                <e:combo id="sex" classStyle="long">
                                    <div style="color:#99BBE8;background:#fafafa;padding:5px;">Select a sex</div>
                                    <div style="padding:10px">
                                        <input type="radio" name="sex"> 男<br/>
                                        <input type="radio" name="sex"> 女
                                    </div>
                                </e:combo>
                                <label class="right">学历：</label>
                                <e:comboBox id="xl" classStyle="long">
                                    <e:selectItem itemValue="" itemLabel="请选择"/>
                                    <e:selectItems value="<%=demo.getSelectItems() %>"/>
                                </e:comboBox>
                            </div>
                            <div class="input-control">
                                <label class="right">年龄：</label>
                                <e:inputNumber id="age" precision="0" min="0" max="100" classStyle="long"/>
                                <label class="right">出生日期：</label>
                                <e:inputDate id="birthday" classStyle="long"/>
                            </div>
                            <div class="input-control">
                                <label class="right">注册时间：</label>
                                <e:inputDateTime id="registerTime" classStyle="long"/>
                                <label class="right">Score：</label>
                                <e:inputNumberSpinner id="spinner" value="0" increment="10" min="-10" max="100"/>
                                <e:inputDateTimeSpinner id="dts"/>
                            </div>

                            <div class="input-control">
                                <label class="right">下拉树：</label>
                                <e:comboTree id="comboTree" data="<%=nodeList %>" classStyle="long"/>
                                <label class="right">下拉表格：</label>

                            </div>
                        </e:form>
                    </e:panel>
                </e:tab>
                <e:tab title="Test" style="padding:10px;">
                    <e:panel id="t-panel" style="width:380px;padding:5px;">
                        <e:button id="home" text="Home" plain="true"/>
                        <e:menuButton id="edit" text="Edit" iconCls="icon-edit">
                            <e:menuitem text="Undo" iconCls="icon-undo"/>
                            <e:menuitem text="Redo" iconCls="icon-redo"/>
                            <e:separator />
                            <e:submenu label="Toolbar">
                                <e:menuitem text="Link" />
                                <e:menuitem text="Address" />
                            </e:submenu>
                        </e:menuButton>
                        <e:menuButton id="help" text="Help" iconCls="icon-help">
                            <e:menuitem text="Help" />
                            <e:menuitem text="Update" />
                        </e:menuButton>
                        <e:menuButton id="about" text="About" custom="true">
                            <img src="http://www.jeasyui.com/images/logo1.png" style="width:150px;height:50px">
                            <p style="font-size:14px;color:#444;">Try jQuery EasyUI to build your modern, interactive, javascript applications.</p>
                        </e:menuButton>
                    </e:panel>
                </e:tab>
            </e:tabs>
        </e:layoutUnit>
    </e:layout>
</e:body>
</html>
