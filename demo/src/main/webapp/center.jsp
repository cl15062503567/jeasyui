﻿<%--
  Created by IntelliJ IDEA.
  User: JiangFeng
  Date: 2014/8/18
  Time: 13:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="e" uri="org.topteam/easyui" %>

<e:templateOverride name="head">
    <style>
        .home{
            padding:20px;
        }
        .home h1{
            margin: 20px 0px;
        }
        .home p{
            margin: 10 0px;
        }
        .home h3{
            margin-top: 15px;
            margin-bottom: 10px;
        }
        .home ul{
            margin-left: 30px;
        }

        .home ul li{
            margin-top: 8px;
        }
    </style>
</e:templateOverride>
<e:templateOverride name="body">
    <div class="home">
        <h1>Welcome to JSP JEasyUI</h1>
        <p>JEasyUI 是一款JSP控件库。基于jQuery EasyUI构建，也就是前端的渲染部分由EasyUI完成。不对EasyUI作任何修改，并且会跟随EasyUI的版本更新。</p>
        <h3>JEasyUI的目的</h3>
        <ul>
            <li>简化JSP中使用的EasyUI的复杂度</li>
            <li>直接绑定Java对象到控件</li>
            <li>通过JSP的tld文件，让IDE支持标签属性的提示</li>
            <li>通过xml格式的控件写法，简化html+js书写EasyUI控件的代码量。降低出错的概率</li>
            <li>提供Event, EventListener, Param等控件简化EasyUI事件处理</li>
            <li>提供js全局对象jeasyui简化EasyUI的api。在操作控件的时候，无需关系控件的类型</li>
        </ul>

        <h3 style="color: #8B0000;">目前控件基本完成，在建设演示站，同时完善代码...</h3>

        <h3>GIT</h3>
        <ul>
            <li><a href="http://git.oschina.net/for-1988/jeasyui" target="_blank">http://git.oschina.net/for-1988/jeasyui</a></li>
            <li><a href="https://coding.net/u/for/p/jeasyui/git" target="_blank">https://coding.net/u/for/p/jeasyui/git</a></li>
        </ul>
        <h3>About Me</h3>
        <ul>
            <li><a href="http://my.oschina.net/FengJ" target="_blank">http://my.oschina.net/FengJ</a></li>
        </ul>

    </div>

</e:templateOverride>

<e:templateOverride name="script">
    <script type="text/javascript">
    </script>
</e:templateOverride>

<%@include file="_TagsDemoTemplate.jsp" %>
