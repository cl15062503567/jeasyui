<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="e" uri="org.topteam/easyui" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="JEasyUI - 简单、高效的JSP框架，让EasyUI开发更迅速、简单。">
    <meta name="keywords"
          content="JSP, EasyUI, Component">
    <meta name="author" content="姜枫 <for-eleven@hotmail.com>">

    <title>

        文档 &middot; EasyUI for JSP

    </title>

    <!-- Bootstrap core CSS -->
    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Optional Bootstrap Theme -->
    <link href="data:text/css;charset=utf-8,"
          data-href="static/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet"
          id="bs-theme-stylesheet">

    <link href="static/css/patch.css" rel="stylesheet">

    <!-- Documentation extras -->
    <link href="static/css/docs.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="static/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="static/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="icon" href="/favicon.ico">
    <script src="static/jquery.min.js"></script>
    <script>
        var _hmt = _hmt || [];
    </script>
    <e:resources location="static/easyui1.4.1" theme="metro"/>
</head>
<body>
<a class="sr-only sr-only-focusable" href="#content">Skip to main content</a>

<!-- Docs master nav -->
<header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse"
                    data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand">JEasyUI</a>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.jsp">首页</a>
                </li>
                <li>
                    <a href="demo.jsp" target="_blank">演示</a>
                </li>
                <li class="active">
                    <a href="document.jsp">文档</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">源码 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Git@OSC</a></li>
                        <li><a href="#">Git@Coding</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://blog.getbootstrap.com"
                       onclick="_hmt.push(['_trackEvent', 'docv3-navbar', 'click', 'doc-home-navbar-blog'])"
                       target="_blank">关于</a></li>
            </ul>
        </nav>
    </div>
</header>


<!-- Docs page layout -->
<div class="bs-docs-header" id="content">
    <div class="container">
        <h1>文档</h1>

        <p>实现EasyUI所有控件的所有属性，并提供了Fect、Event、EventListener标签来处理控件的事件。</p>

    </div>
</div>

<div class="container bs-docs-container">

    <div class="row">
        <div class="col-md-9" role="main">
            <div class="bs-docs-section">
                <h1 id="begin" class="page-header">开始</h1>

                <h3 id="begin-download">下载</h3>

                <p>JEasyUI目前只提供了源码自助编译安装，目前代码托管于Oschina以及Coding上面。</p>

                <div class="highlight">
                    <pre><code class="html"><span class="nt">http://git.oschina.net/for-1988/jeasyui</span></code></pre>
                </div>
                <div class="highlight">
                    <pre><code class="html"><span class="nt">https://coding.net/for/jeasyui.git</span></code></pre>
                </div>


                <h3 id="begin-dependencies">依赖</h3>

                <p>JEasyUI需要JAVA 6+ 运行环境以及依赖了com.alibaba.fastjson来处理对象转JSON。其他可选依赖可以参照下面的maven配置</p>

                <div class="highlight">
                    <pre><code class="html"><span class="nt">
            &lt;dependency>
                &lt;groupId>javax&lt;/groupId>
                &lt;artifactId>javaee-api&lt;/artifactId>
                &lt;version>6.0&lt;/version>
                &lt;scope>provided&lt;/scope>
            &lt;/dependency>
            &lt;dependency>
                &lt;groupId>com.alibaba&lt;/groupId>
                &lt;artifactId>fastjson&lt;/artifactId>
                &lt;version>1.1.40&lt;/version>
            &lt;/dependency>
            &lt;dependency>
                &lt;groupId>org.atmosphere&lt;/groupId>
                &lt;artifactId>atmosphere-runtime&lt;/artifactId>
                &lt;version>2.2.0&lt;/version>
                &lt;scope>provided&lt;/scope>
            &lt;/dependency>
            &lt;dependency>
                &lt;groupId>org.slf4j&lt;/groupId>
                &lt;artifactId>slf4j-simple&lt;/artifactId>
                &lt;version>1.6.1&lt;/version>
                &lt;scope>provided&lt;/scope>
            &lt;/dependency></span></code></pre>
                </div>

                <h3 id="begin-example">实例</h3>

                <p>一个简单的使用JEasyUI的JSP页面。</p>

                <div class="highlight">
                    <pre><code class="html"><span class="nt">
            &lt;%@ page contentType="<span class="s">text/html;charset=UTF-8</span>" language="<span
                            class="s">java</span>" %>
            &lt;%@taglib prefix="<span class="s">e</span>" uri="<span class="s">org.topteam/easyui</span>" %>
            &lt;!DOCTYPE html>
            &lt;html lang="<span class="s">zh-CN</span>">
            &lt;head>
                &lt;meta charset="<span class="s">UTF-8</span>">
                &lt;title>jeasyui - The JSP component library for EasyUI &lt;/title>
                &lt;script type="<span class="s">text/javascript</span>" src="<span
                            class="s">static/jquery.min.js</span>">&lt;/script>
                &lt;e:resources location="<span class="s">static/easyui1.4.1</span>" theme="<span class="s">metro</span>"/>
            &lt;/head>
            &lt;e:body>
                &lt;e:button <span class="na">id</span>="<span class="s">btn</span>" text="<span
                            class="s">Hello JEasyUI</span>"/>
            &lt;/e:body>
            &lt;/html>
                    </span></code></pre>
                </div>
            </div>

            <div class="bs-docs-section">
                <h1 id="component" class="page-header">控件</h1>

                <p class="lead">基本实现了EasyUI的所有控件，在其基础上添加了部分新控件。
                </p>

                <h3 id="component-Accordion">Accordion</h3>

                <p>Accordion由EasyUI提供，实现了其所有的属性。</p>

                <div class="bs-example">
                    <img src="static/images/acc.png"
                         style="height: 212px; width: 628px; display: block;">
                </div>
                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:accordion</span> id="<span class="s">acc</span>" style="<span
                        class="s">width:600;height:200px</span>">
     &lt;<span class="s">e:tab</span> title="<span class="s">About</span>" iconCls="<span class="s">icon-ok</span>" style="<span
                        class="s">padding:10px</span>">
         &lt;h3>EasyUI Accordion&lt;/h3>
         &lt;p>Accordion is a part of easyui framework for jQuery. It lets you define your accordion component on web page more easily&lt;/p>
     &lt;/<span class="s">e:tab</span>>
     &lt;<span class="s">e:tab</span> title="<span class="s">Help</span>" iconCls="<span class="s">icon-help</span>" style="<span
                        class="s">padding:10px</span>">
        ...
       &lt;/e:tab>
     &lt;<span class="s">e:tab</span> title="<span class="s">TreeMenu</span>" iconCls="<span
                        class="s">icon-search</span>" style="<span class="s">padding:10px</span>">
        ...
    &lt;/<span class="s">e:tab</span>>
&lt;/<span class="s">e:accordion</span>></span>
                </code></pre>
                </div>
                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>fit</td>
                            <td>Boolean</td>
                            <td>EasyUI属性，是否根据父控件的大小自适应布局，默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>border</td>
                            <td>Boolean</td>
                            <td>是否显示边框</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>animate</td>
                            <td>Boolean</td>
                            <td>是否显示切换动画</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>multiple</td>
                            <td>Boolean</td>
                            <td>是否可以同时显示多个Tab内容</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>selected</td>
                            <td>int</td>
                            <td>设置初始化时打开tab的下标，默认为0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>


                <h3 id="component-Body">Body</h3>

                <div class="bs-callout bs-callout-warning">
                    <h4>必须使用该控件来替代HTML的body标签</h4>

                    <p>JEasyUI通过<code>&lt;e:body>...&lt;/e:body></code>输出了所有页面控件初始化的相关js代码。所以每个页面必须使用该标签。</p>
                </div>
                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;html>
&lt;head>
...
&lt;/head>
&lt;<span class="s">e:body</span>>
...
&lt;<span class="s">/e:body</span>>
&lt;/html>
</span>
                </code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>full</td>
                            <td>Boolean</td>
                            <td>是否使用全屏布局，如果是true的话，可以直接包含<code>&lt;e:layout fit="true"></code>来布局</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>animation</td>
                            <td>Boolean</td>
                            <td>是否开启页面加载过程动画，默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>loadingText</td>
                            <td>String</td>
                            <td>加载动画的显示文字，默认为<code>加载中···</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>closeDuration</td>
                            <td>int</td>
                            <td>加载动画关闭延迟，默认 <code>500</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-BooleanCheckbox">BooleanCheckbox</h3>

                <p>新增加控件，单选Checkbox控件，可以直接绑定java POJO对象布尔类型的属性值。</p>

                <div class="bs-example">
                    <e:selectBooleanCheckbox id="bck-demo" value="${true}"/>是否启用
                </div>
                <!-- /example -->
                <div class="highlight"><pre><code class="html"><span class="nt">&lt;<span class="s">e:selectBooleanCheckbox</span> id="<span
                        class="s">bck-demo</span>" value="<span class="s">&#36;{true}</span>" />是否启用</span>
                </code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>name</td>
                            <td>String</td>
                            <td>对应渲染后HTML的input控件的name值，默认与id相等。</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>value</td>
                            <td>Boolean</td>
                            <td>控件的绑定值，可以是Java Bean的布尔类型的属性。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Button">Button</h3>

                <p>按钮控件，对应EasyUI的linkbutton。</p>

                <div class="bs-example">
                    <img src="static/images/button.png"
                         style="height: 409px; width: 645px; display: block;">
                </div>
                <!-- /example -->
                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:panel</span> id="<span class="s">Basic</span>" border="<span class="s">false</span>">
    &lt;<span class="s">e:button</span> id="<span class="s">add-btn</span>" text="<span
                        class="s">Add</span>" iconCls="<span class="s">icon-add</span>"/>
    &lt;<span class="s">e:button</span> id="<span class="s">remove-btn</span>" text="<span class="s">Remove</span>" iconCls="i<span
                        class="s">con-remove</span>" iconAlign="<span class="s">left</span>"/>
    &lt;<span class="s">e:button</span> id="<span class="s">save-btn</span>" text="<span class="s">Save</span>" iconCls="<span
                        class="s">icon-save</span>"/>
    &lt;<span class="s">e:button</span> id="<span class="s">cut-btn</span>" text="<span
                        class="s">Cut</span>" iconCls="<span class="s">icon-cut</span>" disabled="<span
                        class="s">true</span>"/>
    &lt;<span class="s">e:button</span> id="<span class="s">text-btn</span>" text="<span class="s">Text Button</span>"/>
    &lt;<span class="s">e:button</span> id="<span class="s">target-btn</span>" text="<span class="s">About Me</span>" href="<span
                        class="s">http://my.oschina.net/FengJ</span>" iconCls="<span class="s">icon-man</span>"/>
&lt;/<span class="s">e:panel</span>></span>
                </code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>disabled</td>
                            <td>Boolean</td>
                            <td>是否禁用。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>toggle</td>
                            <td>Boolean</td>
                            <td>是否开启按钮状态选择。</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>selected</td>
                            <td>Boolean</td>
                            <td>是否选中，默认false。</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>group</td>
                            <td>String</td>
                            <td>按钮组名称</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>plain</td>
                            <td>Boolean</td>
                            <td>是否显示为内嵌的平滑效果。默认<code>false</code>。</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>text</td>
                            <td>String</td>
                            <td>按钮文字。</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>iconCls</td>
                            <td>String</td>
                            <td>按钮图标样式，添加了<span class="s">Font Awesome 4.0 </span>的支持。ex: iconCls="<span class="s">fa fa-key</span>"</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>iconAlign</td>
                            <td>String</td>
                            <td>按钮图标的位置。</td>
                        </tr>
                         <tr>
                            <td>11</td>
                            <td>size</td>
                            <td>String</td>
                            <td>按钮的大小，支持<code>small</code>,<code>large</code>，默认<code>small</code>。</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>href</td>
                            <td>String</td>
                            <td>对应a标签的href。</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>target</td>
                            <td>String</td>
                            <td>打开新页面的方式。</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>onclick</td>
                            <td>String</td>
                            <td>按钮的onclick事件。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Columns">Columns</h3>

                <p>Columns标签目前只用于 <code>&lt;e:datagrid&gt;</code> 表格控件。可实现表头冻结，复合表头等效果。</p>

                <div class="bs-example">
                    见<a href="#component-DataGrid"><span class="s">DataGrid</span></a>
                </div>
                
                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>frozen</td>
                            <td>Boolean</td>
                            <td>是否冻结表头</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Column">Column</h3>

                <p>Column标签目前只用于 <code>&lt;e:datagrid&gt;</code> 表格控件。必须包含在<code>&lt;e:columns></code>内。</p>

                <div class="bs-example">
                    见<a href="#component-DataGrid"><span class="s">DataGrid</span></a>
                </div>
                
                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>title</td>
                            <td>String</td>
                            <td>表头列名</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>field</td>
                            <td>String</td>
                            <td>对应json数据的字段名。</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>width</td>
                            <td>String</td>
                            <td>一列的宽度。<span class="s">EasyUI 1.4</span>以后支持百分比。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>rowspan</td>
                            <td>int</td>
                            <td>该表头跨越的行数，用在实现复合表头的时候。</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>colspan</td>
                            <td>int</td>
                            <td>该表头跨越的行数，用在实现复合表头的时候。</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>align</td>
                            <td>String</td>
                            <td>该列数据水平对齐方式，可选值<code>left</code>,<code>right</code>,<code>center</code>，默认<code>left</code>。</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>halign</td>
                            <td>String</td>
                            <td>该表头文字水平对齐方式，可选值<code>left</code>,<code>right</code>,<code>center</code>，默认<code>left</code>。</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>sortable</td>
                            <td>Boolean</td>
                            <td>是否可以。</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>order</td>
                            <td>String</td>
                            <td>默认的排序方式，<code>asc</code> 或者 <code>desc</code></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>resizable</td>
                            <td>Boolean</td>
                            <td>是否可以改变列的宽度。</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>fixed</td>
                            <td>Boolean</td>
                            <td>设置为true可以在当datagrid的fitColumns为true时不改变宽度。</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>hidden</td>
                            <td>Boolean</td>
                            <td>控制该列的显隐。</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>checkbox</td>
                            <td>Boolean</td>
                            <td>是否在第一列显示checkbox。</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>formatter</td>
                            <td>String</td>
                            <td>单元格的格式化方法。可以是一个方法名，或者一个function。</td>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td>styler</td>
                            <td>String</td>
                            <td>自定义单元格样式方法。该js方法可以返回一个预定于的css名或者style.</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>sorter</td>
                            <td>String</td>
                            <td>对于本地json数据，自定义排序方式。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Combo">Combo</h3>
                <p>Combo控件实现EasyUI的控件。</p>
                <div class="bs-example">
                    <img src="static/images/Combo.png"
                         style="height: 129px; width: 228px; display: block;">
                </div>
 <div class="highlight"><pre><code class="html"><span class="nt">
 &lt;<span class="s">e:combo</span> id="<span class="s">sex-t</span>" classStyle="<span class="s">long</span>">
    &lt;div style="color:#99BBE8;background:#fafafa;padding:5px;">Select a sex&lt;/div>
    &lt;div style="padding:10px">
      &lt;input type="radio" name="sex"> 男&lt;br/>
      &lt;input type="radio" name="sex"> 女
    &lt;/div>
 &lt;<span class="s">/e:combo</span>>
 </span></code></pre>
                </div>
                <div class="bs-example property">
                    <p>Combo的文本框的属性继承于TextBox控件，@See <code><a href="#component-TextBox">TextBox</a></code></p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>panelWidth</td>
                            <td>int</td>
                            <td>下拉框的宽度，默认与文本框一样宽。</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>panelHeight</td>
                            <td>String</td>
                            <td>下拉框的高度，默认auto。</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>panelMinWidth</td>
                            <td>int</td>
                            <td>下拉框的最小宽度。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>panelMaxWidth</td>
                            <td>int</td>
                            <td>下拉框的最大宽度。</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>panelMinHeight</td>
                            <td>int</td>
                            <td>下拉框的最小高度。</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>panelMaxHeight</td>
                            <td>int</td>
                            <td>下拉框的最大高度。</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>panelAlign</td>
                            <td>String</td>
                            <td>下拉框内容的对齐方式，可选<code>left</code>, <code>right</code>。默认<code>left</code>。</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>multiple</td>
                            <td>Boolean</td>
                            <td>是否支持多选。默认false。</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>selectOnNavigation</td>
                            <td>Boolean</td>
                            <td>是否开启根据文本框输入的值赛选结果。默认<code>false</code>。</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>separator</td>
                            <td>String</td>
                            <td>多选时值的分隔符。默认<code>,</code> 。</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>hasDownArrow</td>
                            <td>Boolean</td>
                            <td>是否显示下拉箭头图标。默认<code>true</code> 。</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>delay</td>
                            <td>int</td>
                            <td>下拉框显示动画延迟。默认<code>200</code> 。</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>keyHandler</td>
                            <td>Expression</td>
                            <td>json对象表达式，键盘事件监听。ex:<code>
                            {up: function(e){},
                                down: function(e){},
                                left: function(e){},
                                right: function(e){},
                                enter: function(e){},
                                query: function(q,e){}
                            }</code> 。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>


                <h3 id="component-ComboBox">ComboBox</h3>

                <p>ComboBox控件实现EasyUI的控件，添加了绑定Java对象数据。</p>

                <div class="bs-example">
                    <img src="static/images/ComboBox.png"
                         style="height: 225px; width: 150px; display: block;">
                </div>
                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:comboBox</span> id="<span class="s">city</span>" url="<span class="s">${root}/frame/zone/queryCity.action?code=320000</span>" value="<span class="s">&#36;{zoneCode}</span>" textField="<span class="s">zoneName</span>" valueField="<span class="s">zoneCode</span>" panelHeight="<span class="s">200</span>">
    &lt;<span class="s">e:eventListener </span> event="<span class="s">onSelect</span>" listener="<span class="s">onCitySelect</span>"/>
&lt;<span class="s">/e:comboBox</span>>
 </span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>ComboBox继承于Combo控件,新增或者重新定义了一下属性,@See <code><a href="#component-Combo">Combo</a></code></p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>valueField</td>
                            <td>String</td>
                            <td>将加载的数据的value字段作为控件的value值，默认<code>value</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>textField</td>
                            <td>String</td>
                            <td>将加载的数据的text字段作为控件的文本值，默认<code>text</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>groupField</td>
                            <td>String</td>
                            <td>那个字段来作为分组依据。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>mode</td>
                            <td>String</td>
                            <td>定义当控件文本值改变时如何加载数据，默认<code>local</code>。如果设置为<code>remote</code>则会将文本值作为参数<code>q</code>的值去请求服务端。</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>url</td>
                            <td>String</td>
                            <td>加载远程数据的URL。</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>method</td>
                            <td>String</td>
                            <td>请求远程的HTTP方式，默认<code>post</code>。</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>data<code>*</code></td>
                            <td>Object</td>
                            <td>支持直接绑定java集合对象。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-ComboGrid">ComboGrid</h3>

                <p>下拉表格ComboGrid控件实现EasyUI的控件，由Combo扩展结合了DataGrid控件实现。</p>

                <div class="bs-example">
                    <img src="static/images/ComboGrid.png"
                         style="height: 343px; width: 413px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:comboGrid</span> id="<span class="s">cateIcon</span>" url="<span class="s">&#36;{root}/abc/icon/table.action</span>" idField="<span class="s">iconName</span>" textField="<span class="s">iconName</span>" fitColumns="<span class="s">true</span>" pagination="<span class="s">true</span>">
    &lt;<span class="s">e:columns</span>>
        &lt;<span class="s">e:column</span> field="<span class="s">iconName</span>" title="<span class="s">图标名称</span>" width="<span class="s">20</span>"/>
        &lt;<span class="s">e:column</span> field="<span class="s">iconType</span>" title="<span class="s">图标类型</span>" width="<span class="s">10</span>"/>
        &lt;<span class="s">e:column</span> field="<span class="s">id2</span>" title="<span class="s">图标</span>" width="<span class="s">10</span>" formatter="<span class="s">iconExample</span>"/>
    &lt;<span class="s">/e:columns</span>>
&lt;<span class="s">/e:comboGrid</span>>
 </span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>ComboGrid继承于<a href="#component-Combo"<code>Combo</code></a>控件和<a href="#component-DataGrid"<code>DataGrid</code></a>控件，基本包含它们的属性，新增或者重新定义了一下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>loadMsg</td>
                            <td>String</td>
                            <td>加载远程数据过程显示的文字。</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>idField</td>
                            <td>String</td>
                            <td>将加载的数据的id字段作为控件的值，<code>必填</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>textField</td>
                            <td>String</td>
                            <td>那个字段来作为选择数据后，显示在Combo控件上的值<code>必填</code>。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>filter</td>
                            <td>function</td>
                            <td>js方法来过滤本地json数据。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-ComboTree">ComboTree</h3>

                <p>下拉树ComboTree控件实现EasyUI的控件，由Combo扩展结合了Tree控件实现。</p>

                <div class="bs-example">
                    <img src="static/images/ComboTree.png"
                         style="height: 243px; width: 284px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:comboTree</span> id="<span class="s">parentId</span>" url="<span class="s">&#36;{root}/frame/zone/zoneTree.action</span>" value="<span class="s">&#36;{pId}</span>"/>
 </span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>ComboTree继承于<a href="#component-Combo"<code>Combo</code></a>控件和<a href="#component-Tree"<code>Tree</code></a>控件，基本包含它们的属性，新增或者重新定义了一下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>editable</td>
                            <td>Boolean</td>
                            <td>是否直接输入文本值。默认<code>false</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-DataGrid">DataGrid</h3>

                <p>表格控件DataGrid是EasyUI非常重要，十分常用的一个控件，主要用来展示数据。在布局方面继承于<code><a href="#component-Panel">Panel</a></code>控件。</p>

                <div class="bs-example">
                    <img src="static/images/DataGrid.png"
                         style="height: 381px; width: 736px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:datagrid</span> id="<span class="s">dg</span>" url="<span class="s">&#36;{root}/abc/icon/table.action</span>" fit="<span class="s">true</span>" fitColumns="<span class="s">true</span>" pagination="<span class="s">true</span>">
    &lt;<span class="s">e:columns</span>>
        &lt;<span class="s">e:column</span> field="<span class="s">ck</span>" checkbox="<span class="s">true</span>"/>
        &lt;<span class="s">e:column</span> field="<span class="s">iconName</span>" title="<span class="s">图标名称</span>" width="<span class="s">50</span>"/>
        &lt;<span class="s">e:column</span> field="<span class="s">iconType</span>" title="<span class="s">图标类型</span>" width="<span class="s">10</span>"/>
        &lt;<span class="s">e:column</span> field="<span class="s">id2</span>" title="<span class="s">图标</span>" width="<span class="s">10</span>" formatter="<span class="s">iconExample</span>"/>
        &lt;<span class="s">e:column</span> field="<span class="s">iconId</span>" title="<span class="s">操作</span>" formatter="<span class="s">handleBtn</span>" width="<span class="s">10</span>"/>
    &lt;<span class="s">/e:columns</span>>

    &lt;<span class="s">e:eventListener</span> event="<span class="s">onLoadSuccess</span>" listener="<span class="s">jeasyui.rendBtn</span>"/>

    &lt;<span class="s">e:facet</span> name="<span class="s">toolbar</span>">
        &lt;<span class="s">e:button</span> id="<span class="s">add-btn</span>" text="<span class="s">新增分类图标</span>" classStyle="<span class="s">c-primary</span>" plain="<span class="s">true</span>" iconCls="<span class="s">fa fa-plus</span>">
            &lt;<span class="s">e:eventListener</span> event="<span class="s">click</span>" listener="<span class="s">openAdd</span>"/>
        &lt;<span class="s">/e:button</span>>
    &lt;<span class="s">/e:facet</span>>
&lt;<span class="s">/e:datagrid</span>>
 </span></code></pre>
                </div>

                <div class="bs-callout bs-callout-warning">
                    <h4>建议使用<code>&lt;e:facet name="toolbar"></code>来替代toolbar属性</h4>

                    <p>Facet控件可以更好的处理相关格式化代码。</p>
                </div>

                 <div class="bs-example property">
                    <p>DataGrid在布局方面继承于<a href="#component-Panel"<code>Panel</code></a>控件，然后新增或者重新定义了以下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>fit</td>
                            <td>Boolean</td>
                            <td>继承于Panel的属性，布局是否重名父控件。默认<code>false</code></td>
                        </tr>
                       <tr>
                            <td>2</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>title</td>
                            <td>String</td>
                            <td>表格标题</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>data</td>
                            <td>Object</td>
                            <td>支持绑定java集合对象到控件。<code>新增属性</code></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>fitColumns</td>
                            <td>Boolean</td>
                            <td>是否自动设置表格每列的宽度保证水平方向不出现滚动条。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>resizeHandle</td>
                            <td>String</td>
                            <td>触发改变列宽度的位置，可选值<code>left</code><code>right</code><code>both</code>。默认<code>right</code></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>autoRowHeight</td>
                            <td>Boolean</td>
                            <td>是否设置根据每行的内容来设置其高度，如果关闭则可以提供加载性能。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td><span style="text-decoration:line-through;color:red;">toolbar</span></td>
                            <td>String</td>
                            <td>表格的工具条，可以是一个json数组或者dom元素的选择器。EasyUI本身的用法详见其官网。
                            </td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>striped</td>
                            <td>Boolean</td>
                            <td>是否显示表格线</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>method</td>
                            <td>String</td>
                            <td>请求远程数据的方式，默认<code>post</code></td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>nowrap</td>
                            <td>Boolean</td>
                            <td>当单元格内容超过长度时，是否自动转行。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>idField</td>
                            <td>String</td>
                            <td>定义数据的哪个字段作为id。</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>url</td>
                            <td>String</td>
                            <td>加载远程数据的url。</td>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td>loadMsg</td>
                            <td>String</td>
                            <td>加载等待时，显示的文字。</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>pagination</td>
                            <td>Boolean</td>
                            <td>是否分页。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>17</td>
                            <td>rownumbers</td>
                            <td>Boolean</td>
                            <td>是否显示行号列。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>18</td>
                            <td>singleSelect</td>
                            <td>Boolean</td>
                            <td>是否只允许单选。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>19</td>
                            <td>ctrlSelect</td>
                            <td>Boolean</td>
                            <td>设为true时，只允许通过按着ctrl点击数据行来多选。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>20</td>
                            <td>checkOnSelect</td>
                            <td>Boolean</td>
                            <td>设为true时，当点击行时会勾选/取消勾选checkbox列。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>21</td>
                            <td>selectOnCheck</td>
                            <td>Boolean</td>
                            <td>设为true时，当勾选/取消勾选checkbox列同时会选中行。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>22</td>
                            <td>pagePosition</td>
                            <td>String</td>
                            <td>设置分页控件的位置，可选<code>top</code>,<code>bottom</code>,<code>both</code>。默认<code>bottom</code></td>
                        </tr>
                        <tr>
                            <td>23</td>
                            <td>pageNumber</td>
                            <td>int</td>
                            <td>初始化时显示的页数。默认<code>1</code></td>
                        </tr>
                        <tr>
                            <td>24</td>
                            <td>pageSize</td>
                            <td>int</td>
                            <td>初始化时一页显示的数据条数。默认<code>10</code></td>
                        </tr>
                        <tr>
                            <td>25</td>
                            <td>pageList</td>
                            <td>String , int[]</td>
                            <td>分页控件每页显示条数设置功能的下拉框数据，支持字符串<code>[10,20,30,40,50]</code>或者java整形数组。默认<code>[10,20,30,40,50]</code></td>
                        </tr>
                        <tr>
                            <td>26</td>
                            <td>queryParams</td>
                            <td>Object</td>
                            <td>当加载远程数据时，偶外添加的参数。</td>
                        </tr>
                        <tr>
                            <td>27</td>
                            <td>sortName</td>
                            <td>String</td>
                            <td>定义默认根据一列排序。</td>
                        </tr>
                        <tr>
                            <td>28</td>
                            <td>sortOrder</td>
                            <td>String</td>
                            <td>排序方式参数，可选<code>asc</code>,<code>desc</code>。默认<code>asc</code></td>
                        </tr>
                        <tr>
                            <td>29</td>
                            <td>multiSort</td>
                            <td>Boolean</td>
                            <td>是否允许多列同时排序。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>30</td>
                            <td>remoteSort</td>
                            <td>Boolean</td>
                            <td>定义是否由远程数据排序。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>31</td>
                            <td>showHeader</td>
                            <td>Boolean</td>
                            <td>是否显示表头。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>32</td>
                            <td>showFooter</td>
                            <td>Boolean</td>
                            <td>是否显示表脚。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>33</td>
                            <td>scrollbarSize</td>
                            <td>int</td>
                            <td>设置每次滚轮滚动的距离。默认<code>18</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-DateBox">DateBox</h3>

                <p>日期输入控件DateBox由一个可编辑的文本框加一个可选择日期的下拉日历框组成。在布局、验证等方面主要集成于<a href="#component-Combo"><code>Combo</code></a>控件。</p>

                <div class="bs-example">
                    <img src="static/images/DateBox.png"
                         style="height: 238px; width: 259px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">&lt;<span class="s">e:inputDate</span> id="<span class="s">t-birthday</span>" classStyle="<span class="s">long</span>"/></span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>DateBox继承于<a href="#component-Combo"<code>Combo</code></a>控件，基本包含它们的属性，新增或者重新定义了以下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>formatter</td>
                            <td>String</td>
                            <td>日期格式化参数，符合java Formatter格式。默认<code>yyyy-MM-dd</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>currentText</td>
                            <td>String</td>
                            <td>当前时间按钮文字。默认<code>Today</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>closeText</td>
                            <td>String</td>
                            <td>关闭按钮文字。默认<code>Close</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>okText</td>
                            <td>String</td>
                            <td>确认选择按钮文字。默认<code>OK</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-DateTimeBox">DateTimeBox</h3>

                <p>日期时间输入控件DateTimeBox继承于<a href="#component-DateBox"><code>DateBox</code></a>控件。添加了时间选择的支持。</p>

                <div class="bs-example">
                    <img src="static/images/DateTimeBox.png"
                         style="height: 264px; width: 197px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">&lt;<span class="s">e:inputDateTime</span> id="<span class="s">t-birthday</span>" classStyle="<span class="s">long</span>"/></span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>日期时间输入控件DateTimeBox继承于<a href="#component-DateBox"><code>DateBox</code></a>控件。，基本包含它们的属性，新增或者重新定义了以下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>showSeconds</td>
                            <td>Boolean</td>
                            <td>是否显示秒数。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>spinnerWidth</td>
                            <td>String</td>
                            <td>时间选择框的宽度。默认<code>100%</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>timeSeparator</td>
                            <td>String</td>
                            <td>时分秒之间的分隔符。默认<code>:</code></td>
                        </tr>
                         <tr>
                            <td>4</td>
                            <td>formatter</td>
                            <td>String</td>
                            <td>日期格式化参数，符合java Formatter格式。默认<code>yyyy-MM-dd HH:mm:ss</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-DateTimeSpinner">DateTimeSpinner</h3>

                <p>时间输入控件DateTimeSpinner继承于<a href="#component-TimeSpinner"><code>TimeSpinner</code></a>控件。添加了日期输入的支持。</p>

                <div class="bs-example">
                    <img src="static/images/DateTimeSpinner.png"
                         style="height: 36px; width: 173px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">&lt;<span class="s">e:inputDateTimeSpinner</span> id="<span class="s">t-birthday</span>" value="<span class="s">&lt;%=new Date()%></span>"/></span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>DateTimeSpinner继承于<a href="#component-TimeSpinner"<code>TimeSpinner</code></a>控件，基本包含它们的属性，新增或者重新定义了以下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>formatter</td>
                            <td>String</td>
                            <td>日期格式化参数，符合java Formatter格式。默认<code>yyyy-MM-dd HH:mm:ss</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Dialog">Dialog</h3>
                <p>对话框控件Dialog继承于<a href="#component-Window"><code>Window</code></a>控件, Dialog在顶部和底部可以定义一组操作按钮。</p>

                <div class="bs-callout bs-callout-warning">
                    <h4>在JS中打开一个Dialog</h4>

                    <p>Dialog更加常用的方式是通过js来打开一个新的页面，并且出现在最顶层的iframe中。详情请见<a href="#jsApi-dialog"><code>Dialog API</code></a></p>
                </div>

                <div class="bs-example">
                    <img src="static/images/Dialog.png"
                         style="height: 424px; width: 616px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:button</span> id="<span class="s">dialog-b</span>" text="<span class="s">open Dialog</span>" >
    &lt;<span class="s">e:event</span> event="<span class="s">click</span>" target="<span class="s">dialog</span>" action="<span class="s">open</span>" />
&lt;<span class="s">/e:button</span>>
&lt;<span class="s">e:dialog</span> id="<span class="s">dialog</span>" title="<span class="s">Dialog</span>" closed="<span class="s">true</span>" closable="<span class="s">true</span>" collapsible="<span class="s">true</span>" draggable="<span class="s">true</span>" modal="<span class="s">true</span>" maximizable="<span class="s">true</span>" style="<span class="s">width:500px;height:400px;</span>">
    Dialog Content.
    &lt;<span class="s">e:facet</span> name="<span class="s">toolbar</span>" >
        &lt;<span class="s">e:button</span> id="<span class="s">dialog-save</span>" text="<span class="s">Save</span>" iconCls="<span class="s">icon-save</span>" />
        &lt;<span class="s">e:button</span> id="<span class="s">dialog-cancel</span>" text="<span class="s">Cancel</span>" iconCls="<span class="s">icon-cancel</span>" />
    &lt;<span class="s">/e:facet</span>>
    &lt;<span class="s">e:facet</span> name="<span class="s">buttons</span>" >
        &lt;<span class="s">e:button</span> id="<span class="s">dialog-save2</span>" text="<span class="s">Save</span>" iconCls="<span class="s">icon-save</span>" />
        &lt;<span class="s">e:button</span> id="<span class="s">dialog-cancel2</span>" text="<span class="s">Cancel</span>" iconCls="<span class="s">icon-cancel</span>" />
    &lt;<span class="s">/e:facet</span>>
&lt;<span class="s">/e:dialog</span>></span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>对话框控件Dialog继承于<a href="#component-Window"<code>Window</code></a>控件，基本包含它们的属性，新增或者重新定义了以下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td><span style="text-decoration:line-through;">toolbar</span></td>
                            <td>String</td>
                            <td>Dom元素ID，建议使用<code>facet</code>标签实现。</td>
                        </tr>
                         <tr>
                            <td>2</td>
                            <td><span style="text-decoration:line-through;">buttons</span></td>
                            <td>String</td>
                            <td>Dom元素ID，建议使用<code>facet</code>标签实现。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Fieldset">Fieldset</h3>
                <p>由于某个项目表单字段太多，为了精简布局，加了这个控件。基于html的fieldset实现。</p>

                <div class="bs-example">
                    <img src="static/images/Fieldset.png"
                         style="height: 119px; width: 696px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:fieldset</span> id="<span class="s">fs</span>" legend="<span class="s">基本信息</span>" toggleable="<span class="s">true</span>" style="<span class="s">width:650px;</span>">
    ... ...
&lt;<span class="s">/e:fieldset</span>></span></code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>legend</td>
                            <td>String</td>
                            <td>对应html legend的值。<code>必填</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>closed</td>
                            <td>Boolean</td>
                            <td>默认初始化时，内容是否为隐藏状态。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>toggleable</td>
                            <td>Boolean</td>
                            <td>是否开启点击legend的时候，隐藏/显示内容功能。默认<code>false</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-FileBox">FileBox</h3>
                <p>文件选择控件继承于<a href="#component-TextBox"><code>TextBox</code></a>控件。用于上传附件时，只能和form一起提交。注意设置form的数据类型<code>enctype="multipart/form-data"</code></p>

                <div class="bs-example">
                    <img src="static/images/InputFile.png"
                         style="height: 46px; width: 401px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:inputFile</span> id="<span class="s">input-file</span>" buttonText="<span class="s">选择文件</span>" style="<span class="s">width:300px;</span>" required="<span class="s">true</span>" missingMessage="<span class="s">必须上传文件</span>" /></span></code></pre></div>

                <div class="bs-example property">
                    <p>FileBox完全继承于<code>TextBox</code>控件，只是重新定义了下面属性的默认值。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>buttonText</td>
                            <td>String</td>
                            <td>选择文件的按钮名称。<code>Choose File</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>buttonIcon</td>
                            <td>String</td>
                            <td>按钮的图标。</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>buttonAlign</td>
                            <td>String</td>
                            <td>按钮的位置。可选<code>left</code>, <code>right</code>. 默认<code>right</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>


                <h3 id="component-FileUpload">FileUpload</h3>
                <p>增强的附件上传控件，借助<a href="http://www.plupload.com/" target="_blank">Plupload</a>插件实现了异步附件上传。详细可以参考<code>jeasyui.js</code>和<code>org.topteam.ui.easyui.FileUploadTag</code></p>

                <div class="bs-example">
                    <img src="static/images/FileUpload.png"
                         style="height: 105px; width: 186px; display: block;">
                </div>

                <div class="bs-callout bs-callout-warning">
                    <p>需要自行引入Plupload的资源
                    <code>&lt;e:resource location="static/plupload" name="plupload.full.min.js"/></code>
                    </p>
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:fileUpload</span> id="<span class="s">files</span>" url="<span class="s">&#36;{root}/abc/icon/upload.action</span>" filters="<span class="s">jpg,jpeg,gif,png</span>" maxFileSize="<span class="s">1mb</span>" /></span></code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                       <tr>
                            <td>1</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                         <tr>
                            <td>3</td>
                            <td>url</td>
                            <td>String</td>
                            <td>接受附件的后台地址。<code>必填</code></td>
                        </tr>
                         <tr>
                            <td>4</td>
                            <td>filters</td>
                            <td>String</td>
                            <td>可以接受的文件类型后缀，多个用<code>,</code>隔开。ex: <code>jpg,jpeg,gif,png</code></td>
                        </tr>
                         <tr>
                            <td>5</td>
                            <td>basePath</td>
                            <td>String</td>
                            <td>Plupload使用flash或者silverlight上传附件的时候，相关资源的路径。默认<code>../plupload</code></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>multiFile</td>
                            <td>Boolean</td>
                            <td>是否可以一次选择多个文件。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>params</td>
                            <td>Object</td>
                            <td>上传附件提交时，额外添加的参数。</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>chooseBtnText</td>
                            <td>String</td>
                            <td>选择文件按钮的文字。默认<code>选择文件</code></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>uploadBtnText</td>
                            <td>String</td>
                            <td>上传文件按钮的文字。默认<code>上传</code></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>chooseBtnCls</td>
                            <td>String</td>
                            <td>选择文件按钮的自定义样式。</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>uploadBtnText</td>
                            <td>String</td>
                            <td>上传文件按钮的自定义样式。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Form">Form</h3>
                <p>Form控件默认使用EasyUI的form插件。建议所有交互都通过form，尽量避免自己写ajax来交互。</p>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:form</span> id="<span class="s">form</span>" url="<span class="s">&#36;{root}/abc/info/save.action</span>" style="<span class="s">margin-top:20px;</span>">&lt;/<span class="s">e:form</span>></span></code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                       <tr>
                            <td>1</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                         <tr>
                            <td>3</td>
                            <td>method</td>
                            <td>String</td>
                            <td>form提交的方式。默认<code>post</code></td>
                        </tr>
                         <tr>
                            <td>4</td>
                            <td>novalidate</td>
                            <td>Boolean</td>
                            <td>是否取消表单验证。默认<code>false</code></td>
                        </tr>
                         <tr>
                            <td>5</td>
                            <td>ajax</td>
                            <td>Boolean</td>
                            <td>是否通过ajax提交。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>queryParams</td>
                            <td>Object</td>
                            <td>提交表单时额外附加的参数。</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>params</td>
                            <td>Object</td>
                            <td>上传附件提交时，额外添加的参数。</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>data</td>
                            <td>Object</td>
                            <td>直接加载java对象的各个字段到表单的控件。</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>loadUrl</td>
                            <td>String</td>
                            <td>通过调用远程URL返回数据填充表单。<code>详情见EasyUI文档</code></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>multipart</td>
                            <td>Boolean</td>
                            <td>form控件是否支持提交二进制内容。也就是加上<code>enctype="multipart/form-data"</code>配置。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Layout">Layout</h3>
                <p>页面布局控件Layout包括五个区域<code>north, south, east, west, center</code>，Layout可以互相嵌套，这样可以实现比较复杂的布局。</p>

                <div class="bs-example">
                    <img src="static/images/Layout.png"
                         style="height: 363px; width: 663px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:layout</span> id="<span class="s">test-layout</span>" style="<span class="s">width:650px;height:350px</span>">
    &lt;<span class="s">e:layoutUnit</span> region="<span class="s">north</span>" style="<span class="s">height:100px</span>">
        North
    &lt;<span class="s">/e:layoutUnit</span>>
    &lt;<span class="s">e:layoutUnit</span> region="<span class="s">west</span>" style="<span class="s">width:100px</span>">
        West
    &lt;<span class="s">/e:layoutUnit</span>>
    &lt;<span class="s">e:layoutUnit</span> region="<span class="s">east</span>" style="<span class="s">width:100px</span>">
        East
    &lt;<span class="s">/e:layoutUnit</span>>
    &lt;<span class="s">e:layoutUnit</span> region="<span class="s">south</span>" style="<span class="s">height:50px</span>">
        South
    &lt;<span class="s">/e:layoutUnit</span>>
    &lt;<span class="s">e:layoutUnit</span> region="<span class="s">center</span>">
        Center
    &lt;<span class="s">/e:layoutUnit</span>>
&lt;<span class="s">/e:layout></span></code></pre>
                </div>

                 <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                       <tr>
                            <td>1</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                         <tr>
                            <td>3</td>
                            <td>fit</td>
                            <td>Boolean</td>
                            <td>是否根据父控件自适应大小。默认<code>false</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-LayoutUnit">LayoutUnit</h3>
                <p>LayoutUnit是通过region定义布局的一个区域。</p>

                 <div class="bs-example">
                    见<a href="#component-Layout">Layout</a>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                       <tr>
                            <td>1</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                         <tr>
                            <td>3</td>
                            <td>region</td>
                            <td>String</td>
                            <td>布局的区域位置。可选<code>north</code>, <code>south</code>, <code>east</code>, <code>west</code>, <code>center</code>。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>title</td>
                            <td>String</td>
                            <td>区域标题</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>border</td>
                            <td>Boolean</td>
                            <td>是否显示边框。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>split</td>
                            <td>Boolean</td>
                            <td>是否可以改变区域大小。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>iconCls</td>
                            <td>String</td>
                            <td>标题前面显示的图标。默认不显示</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td><span style="text-decoration:line-through;">href</span></td>
                            <td>String</td>
                            <td>加载目标页面的代码。<code>不建议使用</code></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>collapsible</td>
                            <td>Boolean</td>
                            <td>是否可以隐藏/显示该区域按钮。默认<code>false</code></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>minWidth</td>
                            <td>int</td>
                            <td>可以拖动区域时的最小宽度。</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>minHeight</td>
                            <td>int</td>
                            <td>可以拖动区域时的最小高度。</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>maxWidth</td>
                            <td>int</td>
                            <td>可以拖动区域时的最大宽度。</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>maxHeight</td>
                            <td>int</td>
                            <td>可以拖动区域时的最大高度。</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-MenuButton">MenuButton</h3>
                <p>下拉菜单按钮MenuButton基于<a href="#component-Button">Button</a>和<a href="#component-Menu">Menu</a>构建。实现了下拉菜单功能。</p>

                <div class="bs-example">
                    <img src="static/images/MenuButton.png"
                         style="height: 158px; width: 405px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:panel</span> id="<span class="s">t-panel</span>" style="<span class="s">width:380px;padding:5px;</span>">
    &lt;<span class="s">e:button</span> id="<span class="s">home</span>" text="<span class="s">Home</span>" plain="<span class="s">true</span>"/>
    &lt;<span class="s">e:menuButton</span> id="<span class="s">edit</span>" text="<span class="s">Edit</span>" iconCls="i<span class="s">con-edit</span>">
        &lt;<span class="s">e:menuitem</span> text="<span class="s">Undo</span>" iconCls="<span class="s">icon-undo</span>"/>
        &lt;<span class="s">e:menuitem</span> text="<span class="s">Redo</span>" iconCls="<span class="s">icon-redo</span>"/>
        &lt;<span class="s">e:separator</span> />
        &lt;<span class="s">e:submenu</span> label="<span class="s">Toolbar</span>">
            &lt;<span class="s">e:menuitem</span> text="<span class="s">Link</span>" />
            &lt;<span class="s">e:menuitem</span> text="<span class="s">Address</span>" />
        &lt;<span class="s">/e:submenu</span>>
    &lt;<span class="s">/e:menuButton</span>>
    &lt;<span class="s">e:menuButton</span> id="<span class="s">help</span>" text="<span class="s">Help</span>" iconCls="<span class="s">icon-help</span>">
        &lt;<span class="s">e:menuitem</span> text="<span class="s">Help</span>" />
        &lt;<span class="s">e:menuitem</span> text="<span class="s">Update</span>" />
    &lt;<span class="s">/e:menuButton</span>>
    &lt;<span class="s">e:menuButton</span> id="<span class="s">about</span>" text="<span class="s">About</span>" custom="<span class="s">true</span>">
        &lt;img src="http://www.jeasyui.com/images/logo1.png" style="width:150px;height:50px">
        &lt;p style="font-size:14px;color:#444;">Try jQuery EasyUI to build your modern, interactive, javascript applications.&lt;/p>
     &lt;<span class="s">/e:menuButton</span>>
&lt;<span class="s">/e:panel</span>></span></code></pre>
                </div>

                <div class="bs-example property">
                    <p>MenuButton属性继承于<a href="#component-Button"><code>Button</code></a>对象，新增或者重新定义以下属性。</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                       <tr>
                            <td>1</td>
                            <td>plain</td>
                            <td>Boolean</td>
                            <td>按钮是否显示为平滑风格。默认<code>true</code></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>menu</td>
                            <td>String</td>
                            <td>Dom对象的id，作为下拉的内容。</td>
                        </tr>
                         <tr>
                            <td>3</td>
                            <td>menuAlign</td>
                            <td>String</td>
                            <td>菜单对齐方式, 可选<code>left</code>, <code>right</code>。默认<code>left</code></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>duration</td>
                            <td>int</td>
                            <td>下拉动画的延迟时间。默认<code>100</code></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>custom</td>
                            <td>Boolean</td>
                            <td>是否将<code>e:menuButton</code>标签中间的html内容作为下拉框的内容。默认<code>false</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-MenuItem">MenuItem</h3>
                <p>菜单的叶节点，MenuItem不可以在包含子菜单。</p>

                <div class="bs-example">
                    见<a href="#component-MenuButton">MenuButton</a>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                       <tr>
                            <td>1</td>
                            <td>text</td>
                            <td>String</td>
                            <td>菜单名称。</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>href</td>
                            <td>String</td>
                            <td>带有超链接菜单的目标地址。</td>
                        </tr>
                          <tr>
                            <td>3</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>iconCls</td>
                            <td>String</td>
                            <td>菜单图标的样式。</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>name</td>
                            <td>String</td>
                            <td>该菜单的name值，在处理menu事件的时候，来区分按钮。</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>disabled</td>
                            <td>Boolean</td>
                            <td>是否禁用该菜单。默认<code>false</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-Menu">Menu</h3>
                <p>菜单Menu控件，一般用于自定义右击菜单。或者与其他控件组合成其他控件。</p>

                <div class="bs-example">
                    <img src="static/images/menu.png"
                         style="height: 98px; width: 291px; display: block;">
                </div>

                <div class="highlight"><pre><code class="html"><span class="nt">
&lt;<span class="s">e:menu</span> id="<span class="s">menu</span>" show="<span class="s">true</span>" target="<span class="s">acc</span>">
    &lt;<span class="s">e:submenu</span> label="<span class="s">Menu1</span>">
        &lt;<span class="s">e:menuitem</span> text="<span class="s">Menu1.1</span>" href="<span class="s">http://www.baidu.com</span>"/>
    &lt;<span class="s">/e:submenu</span>>
    &lt;<span class="s">e:menuitem</span> text="<span class="s">Menu2</span>"/>
&lt;<span class="s">/e:menu</span>>
                </span></code></pre>
                </div>

                <div class="bs-example property">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>属性名</th>
                            <th>类型</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>classStyle</td>
                            <td>String</td>
                            <td>自定义样式class名称</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>style</td>
                            <td>String</td>
                            <td>控制控件的样式，一般来设定控件的大小。ex: <code>width:300px;height:200px</code></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>zIndex</td>
                            <td>int</td>
                            <td>菜单控件所在css的层级。</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>left</td>
                            <td>int</td>
                            <td>菜单显示的位置。</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>top</td>
                            <td>int</td>
                            <td>菜单显示的位置。</td>
                        </tr>
                         <tr>
                            <td>6</td>
                            <td>minWidth</td>
                            <td>int</td>
                            <td>菜单显示的最小宽度。默认<code>120</code></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>duration</td>
                            <td>int</td>
                            <td>菜单显示/隐藏的动画延迟。</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>hideOnUnhover</td>
                            <td>Boolean</td>
                            <td>是否在鼠标。默认<code>120</code></td>
                        </tr>
                         <tr>
                            <td>9</td>
                            <td>target</td>
                            <td>String</td>
                            <td>作为右击菜单时，绑定到哪个控件的ID。<code style="color:green;">新增</code></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3 id="component-NumberBox">NumberBox</h3>
                <p>数字输入框，该标签完全基于EasyUI的控件实现。NumberBox继承于TextBox在其基础上添加了数字验证。</p>

                
                
            </div>
        </div>
        <div class="col-md-3">
            <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm" role="complementary">
                <ul class="nav bs-docs-sidenav">

                    <li>
                        <a href="#begin">开始</a>
                        <ul class="nav">
                            <li><a href="#begin-download">下载</a></li>
                            <li><a href="#begin-dependencies">依赖</a></li>
                            <li><a href="#begin-example">实例</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#component">控件</a>
                        <ul class="nav">
                            <li><a href="#component-Accordion">Accordion</a></li>
                            <li><a href="#component-Body">*Body</a></li>
                            <li><a href="#component-BooleanCheckbox">*BooleanCheckbox</a></li>
                            <li><a href="#component-Button">Button</a></li>
                            <li><a href="#component-Columns">Columns</a></li>
                            <li><a href="#component-Column">Column</a></li>
                            <li><a href="#component-Combo">Combo</a></li>
                            <li><a href="#component-ComboBox">ComboBox</a></li>
                            <li><a href="#component-ComboGrid">ComboGrid</a></li>
                            <li><a href="#component-ComboTree">ComboTree</a></li>
                            <li><a href="#component-DataGrid">DataGrid</a></li>
                            <li><a href="#component-DateBox">DateBox</a></li>
                            <li><a href="#component-DateTimeBox">DateTimeBox</a></li>
                            <li><a href="#component-DateTimeSpinner">DateTimeSpinner</a></li>
                            <li><a href="#component-Dialog">Dialog</a></li>
                            <li><a href="#component-Fieldset">Fieldset</a></li>
                            <li><a href="#component-FileBox">FileBox</a></li>
                            <li><a href="#component-FileUpload">FileUpload</a></li>
                            <li><a href="#component-Form">Form</a></li>
                            <li><a href="#component-Layout">Layout</a></li>
                            <li><a href="#component-LayoutUnit">LayoutUnit</a></li>
                            <li><a href="#component-MenuButton">MenuButton</a></li>
                            <li><a href="#component-MenuItem">MenuItem</a></li>
                            <li><a href="#component-Menu">Menu</a></li>
                            <li><a href="#component-NumberBox">NumberBox</a></li>
                            <li><a href="#component-NumberSpinner">NumberSpinner</a></li>
                            <li><a href="#component-Panel">Panel</a></li>
                            <li><a href="#component-Resources">Resources</a></li>
                            <li><a href="#component-Resource">Resource</a></li>
                            <li><a href="#component-SearchBox">SearchBox</a></li>
                            <li><a href="#component-SelectItems">SelectItems</a></li>
                            <li><a href="#component-SelectItem">SelectItem</a></li>
                            <li><a href="#component-SelectManyCheckBox">SelectManyCheckBox</a></li>
                            <li><a href="#component-SelectOneRadio">SelectOneRadio</a></li>
                            <li><a href="#component-Separator">Separator</a></li>
                            <li><a href="#component-Slider">Slider</a></li>
                            <li><a href="#component-Spinner">Spinner</a></li>
                            <li><a href="#component-SplitButton">SplitButton</a></li>
                            <li><a href="#component-SubMenu">SubMenu</a></li>
                            <li><a href="#component-Tabs">Tabs</a></li>
                            <li><a href="#component-Tab">Tab</a></li>
                            <li><a href="#component-TemplateBlock">TemplateBlock</a></li>
                            <li><a href="#component-TemplateOverride">TemplateOverride</a></li>
                            <li><a href="#component-TextBox">TextBox</a></li>
                            <li><a href="#component-TimeSpinner">TimeSpinner</a></li>
                            <li><a href="#component-TreeGrid">TreeGrid</a></li>
                            <li><a href="#component-TreeNode">TreeNode</a></li>
                            <li><a href="#component-Tree">Tree</a></li>
                            <li><a href="#component-ValidateBox">ValidateBox</a></li>
                            <li><a href="#component-Window">Window</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#jsApi">JavaScript API</a>
                        <ul class="nav">
                            <li><a href="#jsApi-jeasyui">jeasyui</a></li>
                            <li><a href="#jsApi-dialog">Dialog API</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#panels">面板</a>
                        <ul class="nav">
                            <li><a href="#panels-basic">基本实例</a></li>
                            <li><a href="#panels-heading">带标题的面版</a></li>
                            <li><a href="#panels-alternatives">情境效果</a></li>
                            <li><a href="#panels-tables">带表格的面版</a>
                            <li><a href="#panels-list-group">带列表组的面版</a>
                        </ul>
                    </li>
                    <li><a href="#responsive-embed">具有响应式特性的嵌入内容</a></li>
                    <li><a href="#wells">Well</a></li>


                </ul>
                <a class="back-to-top" href="#top">
                    返回顶部
                </a>

                <a href="#" class="bs-docs-theme-toggle" role="button">
                    主题预览
                </a>

            </div>
        </div>
    </div>
</div>

<!-- Footer
================================================== -->
 <footer class="bs-docs-footer" role="contentinfo">
      <div class="container">
        

        <p>该网站由<a href="http://my.oschina.net/FengJ">@FengJ</a>创建.</p>
        <p>关于EasyUI的使用授权，JEasyUI只是基于EasyUI的GPL版本构建，完全遵循<a href="http://www.gnu.org/licenses/gpl.txt">GPL协议</a>.</p>
        <ul class="bs-docs-footer-links muted">
          <li>当前版本： v1.0.0</li>
          <li>·</li>
          <li><a href="http://git.oschina.net/for-1988">GIT@OSC 仓库</a></li>
          <li>·</li>
          <li><a href="../about/">关于</a></li>
          <li>·</li>
          <li><a href="http://git.oschina.net/for-1988/jeasyui/issues">Issues</a></li>
          <li>·</li>
          <li><a href="http://git.oschina.net/for-1988/jeasyui/tags">历史版本</a></li>
        </ul>
      </div>
    </footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<script src="static/bootstrap/js/bootstrap.min.js"></script>


<script src="static/js/docs.min.js"></script>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="static/js/ie10-viewport-bug-workaround.js"></script>


</body>
</html>

