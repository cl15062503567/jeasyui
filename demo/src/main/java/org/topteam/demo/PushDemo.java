package org.topteam.demo;

import org.topteam.push.PushContext;
import org.topteam.push.PushContextFactory;

/**
 * Created by 枫 on 2014/8/23.
 */
public class PushDemo {

    public void start(){
        PushContext push = PushContextFactory.getDefault().getPushContext();
        int i =0;
        while (true){
            push.push("count", i);
            i++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
