package org.topteam.demo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import org.topteam.ui.model.Expression;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by 枫 on 2014/8/13.
 */
public class Test {

    private static SerializeConfig mapping = new SerializeConfig();

    static {
        mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
        mapping.put(Expression.class,new ObjectSerializer() {
            @Override
            public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType) throws IOException {
                serializer.getWriter().write(object.toString());
            }
        });
    }

    public static void  main(String[] args){
        Expression expression = new Expression("aaa");
        System.out.println(JSON.toJSONString(expression, mapping));
    }
}
