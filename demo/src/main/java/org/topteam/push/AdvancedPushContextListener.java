package org.topteam.push;

import org.atmosphere.cpr.AsyncSupportListener;
import org.atmosphere.cpr.BroadcasterListener;

/**
 * Created by 枫 on 2014/8/23.
 */
public interface AdvancedPushContextListener extends PushContextListener, AsyncSupportListener, BroadcasterListener {
}
