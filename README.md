#jeasyui

jeasyui项目是对Easyui控件库的JSP封装，将easyui组件封装成JSP标签库。简化easyui的使用难度，学习成本，更少的代码量。友好的IDE提示，类似JSF，Asp控件的写法，提供了相对统一的事件处理机制。直接通过jsp中的java对象绑定数据到控件。

演示地址 http://jeasyui.coding.io/

##News
release 0.0.4

##Some Example

datagrid   
```
<e:datagrid id="grid" data="<%=demo.getProducts() %>" fit="true" title="Fluid DataGrid" classStyle="c-red" pagination="true" fitColumns="false" pageList="10,20,40">  
    <e:columns frozen="true">   
        <e:column field="itemid" width="100">Item ID</e:column>   
        <e:column field="productid" width="200" sortable="true">Product</e:column>   
        <e:column field="listprice" width="200">List Price</e:column>   
        <e:column field="unitcost" width="100" title="单价"/>   
        <e:column field="attr1" width="200">Product</e:column>   
        <e:column field="status" width="200">List Price</e:column>   
    </e:columns>   
</e:datagrid>
```
                    
button&event
```
<e:button id="open-win" text="Open Window" classStyle="c-info">
    <e:event event="click" target="window" action="open"/>
</e:button>
```

##更多请查看Wiki