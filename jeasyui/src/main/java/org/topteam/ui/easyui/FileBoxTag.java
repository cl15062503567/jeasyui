package org.topteam.ui.easyui;

/**
 * Created by 枫 on 2014/8/13.
 */
public class FileBoxTag extends TextBoxTag {

    public static final String TAG = "filebox";

    @Override
    public String getEasyuiTag() {
        return TAG;
    }

}
