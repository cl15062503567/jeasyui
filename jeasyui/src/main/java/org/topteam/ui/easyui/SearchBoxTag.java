package org.topteam.ui.easyui;

/**
 * Created by 枫 on 2014/8/20.
 */
public class SearchBoxTag extends TextBoxTag {
    public static final String TAG = "searchbox";

    @Override
    public String getEasyuiTag() {
        return TAG;
    }
}
